
<?php

/* Controlador */

$usuario=$_POST['usuario'];
$contraseña=$_POST['password'];


function comparar($aplicacion)
{
    $datos = array();
    
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/model.php";
    
    /*
     * Obtenemos el *id del autor* que el cliente nos está pasando como parámetro por GET. Cabe notar que no existe validación alguna... es el modelo que se encargará de eso.
     */
    $id_articulo = @$_GET['usuario'];
    
    /* Y le solicitamos al modelo a *ese autor* en particular */
    $resultado = obtener_autor($aplicacion, $id_articulo);
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Autores - Editar autor - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['autores'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Autores - Editar de autor';
        $datos['vista']['cuerpo'] = 'html/autores/modificar_autor.php';
    }
    require "aplicacion/vistas/html/base/base.php";
}

