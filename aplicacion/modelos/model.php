<?php

/* Modelo */
function obtener_autor($aplicacion, $id_articulo)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    /*
     * VALIDACIONES QUE NO REQUIEREN DE ACCESO A LA BASE DE DATOS Si el id del autor que el controlador nos está solicitando no es númerico o excede una longitud de 20 caracteres no debemos perder nuestro tiempo ejecutando una consulta en la base de datos.
     */
    if (! is_numeric($id_articulo) || strlen($id_articulo) > 20) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'El autor no existe .';
    }
    
    /*
     * En otras "acciones" del modelo (distintas a las "acciones" del controlador) las validaciones que no requieren acceso a la base de datos serán más.
     */
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    /* Finalmente, realizamos la "consulta" a la base de datos */
    require_once "aplicacion/librerias/bd/query.php";
    return select_autor($id_articulo);
}
