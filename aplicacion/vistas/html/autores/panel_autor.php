
<div class="panel panel-primary">
	<div class="panel-heading">

	
		Autor: <strong><?php echo   $autor['nombre_autor']; ?></strong>
	</div>
	<div class="panel-body">
		<ul>
			<li><strong>Nacionalidades:</strong> <?php echo $autor['nacionalidad_autor']; ?></li>
		</ul>
		
		<ul>
			<li><strong>id_autor:</strong> <?php echo $autor ['id_autor']; ?></li>
		</ul>
	</div>

	<div class="panel-footer clearfix">
		<div class="pull-right">
			<a href="acceso.php?c=autores&a=editar_autor&id_autor=<?php echo $autor['id_autor']; ?>" class="btn btn-default">Editar</a>
			<a href="acceso.php?c=autores&a=borrar_autor&id_autor=<?php echo $autor['id_autor']; ?>" class="btn btn-warning">Borrar</a>
		</div>
	</div>
</div>



<?php foreach ($datos['autores'] as $autor) { ?>
<div class="panel panel-success">
	<div class="panel-heading">
	
		
		Autor: <strong><?php echo $autor['nombre_autor']; ?></strong>
	</div>
	<div class="panel-body">
	
	<ul>
			<li><strong>ISBN  :</strong> <?php echo $autor['isbn_libro']; ?></li>
		</ul>
	
		<ul>
			<li><strong>Libros Asociados :</strong> <?php echo $autor['titulo_libro']; ?></li>
		</ul>

         <ul>
			<li><strong>Editorial :</strong> <?php echo $autor['editorial_libro']; ?></li>
		</ul>

         <ul>
			<li><strong>Año de publicacion :</strong> <?php echo $autor['anio_publicacion_libro']; ?></li>
		</ul>		
		
	</div>

	<div class="panel-footer clearfix">
		
	</div>
</div>
<?php } ?>

