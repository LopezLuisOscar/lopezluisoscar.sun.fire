
<div class="navbar navbar-inverse" role="navigation">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Biblioteca</span>
				<span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="acceso.php">Biblioteca</a>
		</div>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Autores<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">Lista</li>
						<li><a href="acceso.php?c=autores&a=ver_lista&v=tabla">Tabla de Autores</a></li>
						<li><a href="acceso.php?c=autores&a=ver_lista&v=panel">Panele de Visualizacion</a></li>
						<li class="dropdown-header">Opciones</li>
						<li><a href="acceso.php?c=autores&a=nuevo_autor">Ingresar nuevo autor</a></li>
					</ul>
				</li>

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Ejemplares<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">Lista</li>
						<li><a href="acceso.php?c=ejemplares&a=ver_lista&v=tabla">Tabla de Ejemplares</a></li>
						<li><a href="acceso.php?c=ejemplares&a=ver_lista&v=panel">Paneles de Visualizacion</a></li>
						<li class="dropdown-header">Opciones</li>
						<li><a href="acceso.php?c=ejemplares&a=nuevo_ejemplar">Añadir nuevo Ejemplar</a></li>
					</ul>
				</li>

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Libros<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">Listar</li>
						<li><a href="acceso.php?c=libros&a=ver_lista&v=tabla">Tabla de Libros</a></li>
						<li><a href="acceso.php?c=libros&a=ver_lista&v=panel">Paneles de Visualizacion</a></li>
						<li class="dropdown-header">Opciones</li>
						<li><a href="acceso.php?c=libros&a=nuevo_libro">Ingrese un Nuevo libro</a></li>
							<li><a href="salir.php">SALIRRRR</a></li>
					</ul>
				</li>
				
				
				

			</ul>
			

		</div>
	</div>

</div>
<div   style=margin-left:80%>
				<form  class="form-inline" role="form" method="POST" action="salir.php">
<button type="submit" class="btn btn-danger"  >SALIR DE LA SESION</button>
</form>
</div>