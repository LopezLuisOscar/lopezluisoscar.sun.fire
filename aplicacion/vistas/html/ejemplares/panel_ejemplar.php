<?php foreach ($datos['ejemplares'] as $ejemplar) { ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		Ejemplar: <strong><?php echo $ejemplar['observaciones_ejemplar']; ?></strong>
	</div>
	<div class="panel-body">
		<ul>
			<li><strong>ISBN:</strong> <?php echo $ejemplar['isbn']; ?></li>
		</ul>
	</div>
	

	<div class="panel-footer clearfix">
		<div class="pull-right">
			<a href="acceso.php?c=ejemplares&a=editar_ejemplar&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>" class="btn btn-default">Editar</a>
			<a href="acceso.php?c=ejemplares&a=borrar_ejemplar&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>" class="btn btn-warning">Borrar</a>
		</div>
	</div>
	
	
	<div class="panel-heading">
		<strong>Datos del Libro:</strong>
	</div>
	<div class="panel-body">
		<ul>
			<li><strong>id_ejemplar:</strong> <?php echo $ejemplar['id_ejemplar']; ?></li>
		</ul>
		
		<ul>
			<li><strong>ISBN:</strong> <?php echo $ejemplar['isbn']; ?></li>
		</ul>
		
				<ul>
			<li><strong>Titulo del Libro:</strong> <?php echo $ejemplar['titulo_libro']; ?></li>
		</ul>
		
			<ul>
			<li><strong>Editorial del Libro:</strong> <?php echo $ejemplar['editorial_libro']; ?></li>
		</ul>
		
		<ul>
			<li><strong>Año de publicacion:</strong> <?php echo $ejemplar['anio_publicacion_libro']; ?></li>
		</ul>

	</div>
</div>
<?php } ?>